# PREDICTD

Durham, TJ., Libbrecht, MW., Howbert, JJ., Bilmes, J., and Noble, WS. �PREDICTD PaRallel Epigenomics Data Imputation with Cloud-Based Tensor Decomposition.� Nature Communications 9, no. 1 (April 11, 2018): 1402. https://doi.org/10.1038/s41467-018-03635-9.

This repository contains the code to run PREDICTD, a program to model the epigenome based on the Encyclopedia of DNA Elements and the NIH Roadmap Epigenomics Project data and to impute the results of epigenomics experiments that have not yet been done. A computing environment for running this code is distributed as an Amazon Machine Image, and it is easiest to get the code up and running by following the steps in the tutorial below to start a cluster in Amazon Web Services. This tutorial will demonstrate how to train the model on the Roadmap Consolidated data set used in the paper. The model can also be used to impute data for a new cell type, and there will be another tutorial for that use case coming soon. If you do not want to run the model, but simply want to get the imputed data from the paper, you will be able to download that data in bigwig format from the ENCODE project website soon.

## Installation

PREDICTD is most readily available on Amazon Web Services as part of an Amazon Machine Image (AMI) called PREDICTD (ami-858012fd).

**Update 2023-12-15:** The s3://predictd bucket is being deprecated, and the data stored there has been archived in a Harvard Dataverse Dataset (doi:10.7910/DVN/HNUUSJ). It is still publicly available, but the serialized forms of the Spark Resilient Distributed Datasets (RDDs) have been compressed into gzipped tar archives. Going forward, to run the PREDICTD code against the tutorial data sets or to load the PREDICTD models into a Spark instance, please download the relevant files from the Dataverse Dataset, decompress and untar the RDD files (\*.RDD.pickle), upload them into an AWS S3 bucket, and point the code to read from that new S3 bucket.

## Instructions and Tutorials

For more examples and information about how to run PREDICTD, please see the wiki page of this repository (https://bitbucket.org/noblelab/predictd/wiki/Home).

## License

PREDICTD is released as an open-source project under the MIT License:

MIT License

Copyright (c) 2017 tdurham86

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
